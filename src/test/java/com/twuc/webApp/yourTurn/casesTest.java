package com.twuc.webApp.yourTurn;

import com.twuc.webApp.yourTurn.scanning.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class casesTest {
    private AnnotationConfigApplicationContext context;

    @BeforeEach
    void createContext() {
        context = new AnnotationConfigApplicationContext(
                "com.twuc.webApp.yourTurn.scanning"
        );
    }

    @Test
    void should_get_same_bean_when_object_implements_interface() {
        InterfaceOne bean1 =
                context.getBean(InterfaceOneImpl.class);
        InterfaceOne bean2 = context.getBean((InterfaceOne.class));

        assertEquals(bean1, bean2);
    }

    @Test
    void should_get_same_bean_when_object_extends_a_class() {
        BaseClass base = context.getBean(BaseClass.class);
        ChildClass child = context.getBean(ChildClass.class);

        assertEquals(base, child);
    }

    @Test
    void should_get_same_bean_when_object_extends_a_abstract_class() {
        DerivedClass derivedObj = context.getBean(DerivedClass.class);
        AbstractBaseClass abstractBaseObject = context.getBean(AbstractBaseClass.class);
        assertEquals(derivedObj, abstractBaseObject);
    }

    @Test
    void should_not_get_same_object_when_use_scope_prototype() {
        SimplePrototypeScopeClass bean1 = context.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass bean2 = context.getBean(SimplePrototypeScopeClass.class);
        assertNotEquals(bean1, bean2);
    }

    @Test
    void should_create_singleton_when_scan_and_create_prototype_scope_obj_when_call_get_bean() {
        SimplePrototypeScopeClass bean = context.getBean(SimplePrototypeScopeClass.class);
        context.getBean(SimplePrototypeScopeClass.class);
        List<String> logs = bean.getLogger().getLogs();
        List<String> singleton = logs.stream().filter(v -> v.equals("InterfaceOneImpl")).collect(toList());
        List<String> prototypeScope = logs.stream().filter(v -> v.equals("SimplePrototypeScopeClass")).collect(toList());

        assertEquals(1, singleton.size());
        assertEquals(2, prototypeScope.size());
    }

    @Test
    void should_create_a_new_object_when_call_get_bean() {
        SimplePrototypeScopeClass bean = context.getBean(SimplePrototypeScopeClass.class);
        context.getBean(SimplePrototypeScopeClass.class);

        List<String> logs = bean.getLogger().getLogs();
        List<String> prototypeScope = logs.stream().filter(v -> v.equals("SimplePrototypeScopeClass")).collect(toList());

        assertEquals(2, prototypeScope.size());
    }

    @Test
    void should_create_prototype_scope_object_twice_and_singleton_once() {
        PrototypeScopeDependsOnSingleton bean = context.getBean(PrototypeScopeDependsOnSingleton.class);
        context.getBean(PrototypeScopeDependsOnSingleton.class);

        List<String> logs = bean.getLogger().getLogs();
        List<String> singleton = logs.stream().filter(v -> v.equals("SingletonDependent")).collect(toList());
        List<String> prototypeScope = logs.stream().filter(v -> v.equals("PrototypeScopeDependsOnSingleton")).collect(toList());

        assertEquals(1, singleton.size());
        assertEquals(2, prototypeScope.size());
    }

    @Test
    void should_create_singleton_once_and_prototype_scope_once() {
        SingletonDependsOnPrototype bean = context.getBean(SingletonDependsOnPrototype.class);
        context.getBean(SingletonDependsOnPrototype.class);

        List<String> logs = bean.getLogger().getLogs();
        List<String> singleton = logs.stream().filter(v -> v.equals("SingletonDependsOnPrototype")).collect(toList());
        List<String> prototypeScope = logs.stream().filter(v -> v.equals("PrototypeDependent")).collect(toList());

        assertEquals(1, singleton.size());
        assertEquals(1, prototypeScope.size());
    }

    @Test
    void should_create_new_prototype_when_call_prototype_instance_method() {
        SingletonDependsOnPrototypeProxy bean = context.getBean(SingletonDependsOnPrototypeProxy.class);

        bean.getPrototypeDependentWithProxy().trigger();
        bean.getPrototypeDependentWithProxy().trigger();

        List<String> logs = bean.getLogger().getLogs();
        List<String> collect = logs.stream().filter(log -> log.equals("SingletonDependsOnPrototypeProxy")).collect(toList());
        List<String> collect1 = logs.stream().filter(log -> log.equals("PrototypeDependentWithProxy")).collect(toList());

        assertEquals(1, collect.size());
        assertEquals(2, collect1.size());

    }

    @Test
    void should_create_two_prototype_when_call_toString_of_prototype_twice() {
        SingletonDependsOnPrototypeProxyBatchCall bean = context.getBean(SingletonDependsOnPrototypeProxyBatchCall.class);
        bean.triggerToStringOfDependency();

        List<String> logs = bean.getLogger().getLogs();
        List<String> collect = logs.stream().filter(log -> log.equals("PrototypeDependentWithProxy")).collect(toList());

        assertEquals(2, collect.size());
    }

    @Test
    void should_not_create_target_obj_when_using_proxy_mode() {
        SingletonDependsOnPrototypeProxy bean = context.getBean(SingletonDependsOnPrototypeProxy.class);

        List<String> logs = bean.getLogger().getLogs();
        List<String> collect = logs.stream().filter(log -> log.equals("prototypeDependentWithProxy()")).collect(toList());

        assertEquals(0, collect.size());

    }
}
