package com.twuc.webApp.yourTurn.scanning;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototypeProxy {
    private PrototypeDependentWithProxy prototypeDependentWithProxy;
    private MyLogger logger;

    public SingletonDependsOnPrototypeProxy(PrototypeDependentWithProxy prototypeDependentWithProxy, MyLogger logger) {
        this.prototypeDependentWithProxy = prototypeDependentWithProxy;
        this.logger = logger;
        logger.log("SingletonDependsOnPrototypeProxy");
    }

    public PrototypeDependentWithProxy getPrototypeDependentWithProxy() {
        return prototypeDependentWithProxy;
    }

    public MyLogger getLogger() {
        return logger;
    }
}
