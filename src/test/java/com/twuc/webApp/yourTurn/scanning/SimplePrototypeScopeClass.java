package com.twuc.webApp.yourTurn.scanning;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SimplePrototypeScopeClass {
    private MyLogger logger;

    public SimplePrototypeScopeClass(MyLogger logger) {
        this.logger = logger;
        logger.log("SimplePrototypeScopeClass");
    }

    public MyLogger getLogger() {
        return logger;
    }
}
