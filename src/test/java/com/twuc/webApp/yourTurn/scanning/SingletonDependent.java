package com.twuc.webApp.yourTurn.scanning;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependent {
    private MyLogger logger;

    public SingletonDependent(MyLogger logger) {
        this.logger = logger;
        logger.log("SingletonDependent");
    }
}
