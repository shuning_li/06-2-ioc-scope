package com.twuc.webApp.yourTurn.scanning;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototypeProxyBatchCall {
    private PrototypeDependentWithProxy prototypeDependentWithProxy;
    private MyLogger logger;

    public SingletonDependsOnPrototypeProxyBatchCall(PrototypeDependentWithProxy prototypeDependentWithProxy, MyLogger logger) {
        this.prototypeDependentWithProxy = prototypeDependentWithProxy;
        this.logger = logger;
        logger.log("SingletonDependsOnPrototypeProxyBatchCall");
    }

    public MyLogger getLogger() {
        return logger;
    }

    public void triggerToStringOfDependency() {
        prototypeDependentWithProxy.toString();
        prototypeDependentWithProxy.toString();
    }
}
