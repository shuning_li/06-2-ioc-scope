package com.twuc.webApp.yourTurn.scanning;

import org.springframework.stereotype.Component;

@Component
public class InterfaceOneImpl implements InterfaceOne {
    private MyLogger logger;

    public InterfaceOneImpl(MyLogger logger) {
        this.logger = logger;
        logger.log("InterfaceOneImpl");
    }

    public MyLogger getLogger() {
        return logger;
    }
}
