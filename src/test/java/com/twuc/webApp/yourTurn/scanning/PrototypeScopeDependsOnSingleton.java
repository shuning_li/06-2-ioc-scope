package com.twuc.webApp.yourTurn.scanning;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PrototypeScopeDependsOnSingleton {
    private SingletonDependent singletonDependent;
    private MyLogger logger;

    public PrototypeScopeDependsOnSingleton(SingletonDependent singletonDependent, MyLogger logger) {
        this.singletonDependent = singletonDependent;
        this.logger = logger;
        logger.log("PrototypeScopeDependsOnSingleton");
    }

    public MyLogger getLogger() {
        return logger;
    }
}
