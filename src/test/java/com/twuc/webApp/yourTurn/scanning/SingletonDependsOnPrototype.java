package com.twuc.webApp.yourTurn.scanning;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototype {
    private PrototypeDependent prototypeDependent;
    private MyLogger logger;

    public SingletonDependsOnPrototype(PrototypeDependent prototypeDependent, MyLogger logger) {
        this.prototypeDependent = prototypeDependent;
        this.logger = logger;
        logger.log("SingletonDependsOnPrototype");
    }

    public MyLogger getLogger() {
        return logger;
    }
}
